from django.db import models
from django.contrib.postgres.fields import JSONField


class Channel(models.Model):
    title = models.CharField(max_length=100)
    link = models.CharField(max_length=250)
    description = models.CharField(max_length=250)


class Product(models.Model):
    channel = models.ForeignKey(Channel, null=False, on_delete=models.CASCADE)
    data = JSONField()
