
from django.http import JsonResponse
from django.views.generic import TemplateView

from django.db.models.expressions import RawSQL, OrderBy

from .models import Product
from .serializers import ProductSerializer


class Price(TemplateView):

    def get(self, request, *args, **kwargs):
        products = Product.objects.all().order_by(RawSQL(
            "cast(data->>%s as float)", ("price",)
        ))[:20]
        serializer = ProductSerializer(products, many=True)

        return JsonResponse(serializer.data, safe=False)


class Discount(TemplateView):

    def get(self, request, *args, **kwargs):
        products = Product.objects.all().order_by(RawSQL(
            "cast(data->>%s as float) - cast(data->>%s as float)", ("price", "custom_label_0")
        ))[:20]
        serializer = ProductSerializer(products, many=True)

        return JsonResponse(serializer.data, safe=False)


class MostDiscount(TemplateView):

    def get(self, request, *args, **kwargs):
        products = Product.objects.all().order_by(OrderBy(RawSQL(
            "cast(data->>%s as float) - cast(data->>%s as float)", ("price", "custom_label_0")
        ), descending=True))[:20]
        serializer = ProductSerializer(products, many=True)

        return JsonResponse(serializer.data, safe=False)


class TypeComedy(TemplateView):

    def get(self, request, *args, **kwargs):
        products = Product.objects.extra(
            where=["data->>'product_type' like %s"], params=["%> Comedy >%"])
        serializer = ProductSerializer(products, many=True)

        return JsonResponse(serializer.data, safe=False)
