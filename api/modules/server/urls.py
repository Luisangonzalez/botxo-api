
from django.urls import path

from . import views

urlpatterns = [
    path('price/', views.Price.as_view()),
    path('discount/', views.Discount.as_view()),
    path('discount/most/', views.MostDiscount.as_view()),
    path('type/comedy/', views.TypeComedy.as_view()),
]
