import xmltodict

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from api.modules.server.models import Channel, Product


class Command(BaseCommand):
    help = 'Import test.xml fixtures'

    def _remove_fixtures_in_db(self):
        Channel.objects.all().delete()
        Product.objects.all().delete()

    def handle(self, *args, **options):
        self._remove_fixtures_in_db()

        xml = open(f"{settings.BASE_DIR}/fixtures/test.xml").read()

        data = xmltodict.parse(xml)

        channel = data['rss']['channel']
        products = channel['item']

        try:
            new_channel = Channel.objects.create(
                title=channel['title'],
                link=channel['link'],
                description=channel['description']
            )
            new_channel.save()
        except Exception as ex:
            raise CommandError("Error while import fixture into database") from ex

        for product in products:
            data = product.copy()

            for k, v in product.items():
                new_k = k.replace("g:", "")

                if k.startswith("g:"):
                    del data[k]
                    data[new_k] = v
                
                if new_k == 'price':
                    num, badge = data[new_k].split(" ")
                    data[new_k] = num
                    data['badge'] = badge

            try:
                new_product = Product.objects.create(
                    channel=new_channel,
                    data=data
                )
                new_product.save()
            except Exception as ex:
                raise CommandError("Error while import fixture into database") from ex

        print("[OK] Import complete...")
