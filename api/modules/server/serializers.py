from rest_framework import serializers
from .models import Product, Channel


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = ('title', 'link', 'description',)


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('data',)
