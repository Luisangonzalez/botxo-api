BotXO API Rest (Code Test)
==========================

Write a small app that publish an API Rest with these endpoints:
- Give me 20 products ordered by price.
- Give me 20 products ordered by discount (price - custom_label_0).
- Give me the most discounted 20 products.
- Give me all products under the “Comedy” product type.

## System requirements

- [Docker](https://www.docker.com/)
- [Docker-compose](https://docs.docker.com/compose/install/)
- make

## Python requiremets

- [pipenv](https://github.com/pypa/pipenv)
- [django](https://www.djangoproject.com/download/)
- [djangorestframework](http://www.django-rest-framework.org/)
- [xmltodict](https://github.com/martinblech/xmltodict)


#### Dev requirements

- [flake8](https://pypi.python.org/pypi/flake8)
- [autopep8](https://pypi.python.org/pypi/autopep8)
- [ipdb](https://pypi.python.org/pypi/ipdb)


General
-------

Run server:
```bash
make up
```

Enter into container shell:
```bash
make shell
```

Build docker container:
```bash
make docker_build
```

**IMPORTANT:** Execute the following command, if it's the first time that you run container.

Import fixtures (*fixtures/test.xml*)
```bash
python3 manage.py import_fixtures
```


URLs of the API
---------------

- 20 products ordered by price.

`http://localhost:8000/api/price/`

- 20 products ordered by discount (price - custom_label_0).

`http://localhost:8000/api/discount/`

- The most discounted 20 products.

`http://localhost:8000/api/discount/most/`

- All products under the “Comedy” product type.

`http://localhost:8000/api/type/comedy/`
