.PHONY: stop rm docker_build up shell

# Settings:
PROJECT_NAME=api

# Do not touch
DOCKER_COMPOSE_CMD=docker-compose -p ${PROJECT_NAME} -f environment/docker-compose.yml


default: build

stop:
	${DOCKER_COMPOSE_CMD} stop

rm: stop
	${DOCKER_COMPOSE_CMD} rm -f

docker_build:
	${DOCKER_COMPOSE_CMD} build

up:
	./environment/scripts/check_if_container_exist.sh api_server
	${DOCKER_COMPOSE_CMD} run --rm --name api_server --service-ports api \
		pipenv run ./manage.py runserver 0.0.0.0:8000

shell:
	${DOCKER_COMPOSE_CMD} run --rm api pipenv shell
